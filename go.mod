module github.com/heroku/ginauth

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/lib/pq v1.1.1
	google.golang.org/appengine v1.6.1 // indirect
)
