package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"reflect"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

var signingKey = []byte("savethiskeyinyoursystem")

type nullString string

// Scan implements the Scanner interface for NullString
func (ns *nullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}
	if reflect.TypeOf(value) == nil {
		*ns = nullString("null")
	} else {
		*ns = nullString(s.String)
	}
	return nil
}

type account struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type employee struct {
	Name     string `json:"name"`
	Location string `json:"location"`
}

type testing struct {
	Name     nullString `json:"name"`
	Password nullString `json:"password"`
}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}
	r := setupRouter()
	r.Run(":" + port)
}

func openPostgresDB() (*sql.DB, error) {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	return db, err
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Logger())
	r.LoadHTMLGlob("templates/*.tmpl.html")
	r.GET("/", homePage)
	r.POST("/account", createAccount)
	r.POST("/login", loginAccount)
	r.GET("/employees", auth, getEmployees)
	r.GET("/testing", getTesting)
	return r
}

// gin framework
func homePage(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl.html", nil)
}

func createAccount(c *gin.Context) {
	var acc account
	c.BindJSON(&acc)

	db, err := openPostgresDB()
	if err != nil {
		log.Fatal("Error opening database.")
	}
	defer db.Close()

	_, queryErr := db.Exec("INSERT INTO company (id, name, password) values ($1, $2, $3)", acc.ID, acc.Name, acc.Password)

	if queryErr != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
		c.Abort()
	}

	c.JSON(200, gin.H{
		"status":  "200",
		"message": "success create account",
	})
}

func loginAccount(c *gin.Context) {
	var loggedAcc account
	c.BindJSON(&loggedAcc)

	db, err := openPostgresDB()
	if err != nil {
		log.Fatal("Error opening database.")
	}
	defer db.Close()

	var getAcc account
	queryResult, queryErr := db.Query("SELECT name FROM company where password = $1", loggedAcc.Password)
	if queryErr != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
		c.Abort()
	}
	for queryResult.Next() {
		queryErr = queryResult.Scan(&getAcc.Name)
		if queryErr != nil {
			c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
			c.Abort()
		}
	}
	defer queryResult.Close()

	sign := jwt.New(jwt.SigningMethodHS256)

	claims := sign.Claims.(jwt.MapClaims)

	claims["exp"] = time.Now().Add(time.Second * 15).Unix()

	token, err := sign.SignedString(signingKey)

	if err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
		c.Abort()
	}

	c.JSON(200, gin.H{
		"status":     "200",
		"message":    "succeed logged in with correct account",
		"validToken": token,
	})
}

func getEmployees(c *gin.Context) {
	db, err := openPostgresDB()
	if err != nil {
		log.Fatal("Error opening database.")
	}
	defer db.Close()

	queryResult, queryErr := db.Query("SELECT name, location FROM employee")
	if queryErr != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
		c.Abort()
	}
	defer queryResult.Close()

	empCollection := []employee{}
	var emp employee

	for queryResult.Next() {
		queryErr = queryResult.Scan(&emp.Name, &emp.Location)
		if queryErr != nil {
			c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
			c.Abort()
		}
		empCollection = append(empCollection, emp)
	}

	c.JSON(200, gin.H{
		"status": "200",
		"data":   empCollection,
	})
}

func getTesting(c *gin.Context) {
	db, err := openPostgresDB()
	if err != nil {
		log.Fatal("Error opening database.")
	}
	defer db.Close()

	queryResult, queryErr := db.Query("SELECT name, password FROM testing")
	if queryErr != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
		c.Abort()
	}
	defer queryResult.Close()

	testCollection := []testing{}
	var test testing

	for queryResult.Next() {
		queryErr = queryResult.Scan(&test.Name, &test.Password)
		if queryErr != nil {
			c.String(http.StatusInternalServerError, fmt.Sprintf("Error: %q", queryErr))
			c.Abort()
		}
		testCollection = append(testCollection, test)
	}

	c.JSON(200, gin.H{
		"status": "200",
		"data":   testCollection,
	})
}

func auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("Authorization")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return signingKey, nil
	})

	if token != nil && err == nil {
		fmt.Println("token verified")
	} else {
		result := gin.H{
			"message": "Not Authorized",
			"error":   err.Error(),
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}
